<?php

/**
 * @file
 * Contains \Drupal\node\Controller\AssignmentAnswerController.
 */

namespace Drupal\pe_assignment_answer\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Assignment answer routes.
 */
class AssignmentAnswerController extends ControllerBase {

  /**
   * Handles direct calls to create assignment answers without a question.
   */
  public function add() {
    drupal_set_message(t("You can't manually add an assignment answer without a question context!"), 'error');
    return $this->redirect('node.add_page');
  }

}
