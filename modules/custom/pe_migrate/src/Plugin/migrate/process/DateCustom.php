<?php

/**
 * @file
 * Contains \Drupal\pe_migrate\Plugin\pe_migrate\process\DateCustom.
 */

namespace Drupal\pe_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * This plugin create a datetime field array.
 *
 * @MigrateProcessPlugin(
 *   id = "date_custom",
 *   handle_multiples = TRUE
 * )
 */
class DateCustom extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $timestamp = strtotime($row->getSourceProperty($destination_property));
    $timestamp = ($timestamp) ? $timestamp : REQUEST_TIME;

    $values['value'] = gmdate($this->configuration['format'], $timestamp);

    return $values;
  }
}
