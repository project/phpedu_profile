<?php
  /**
   * @file
   * Enables modules and site configuration for a phpedu_profile site installation.
   */

  use Drupal\Core\Config\FileStorage;
  use Drupal\Core\Config\InstallStorage;
  use Drupal\phpedu_profile\Form\DemoForm;
  use Drupal\Core\Form\FormStateInterface;


  /**
   * Implements hook_install_tasks().
   */
  function phpedu_profile_install_tasks() {
    return array(
      'phpedu_profile_select_demo' => array(
        'display_name' => t('Extensions & Demo'),
        'display'      => TRUE,
        'type'         => 'form',
        'function'     => DemoForm::class,
      ),
      'phpedu_profile_install_demo' => array(
        'display_name' => t('Finish installation'),
        'display'      => TRUE,
        'type'         => 'batch',
      ),
    );
  }

  /**
   * Implements hook_form_FORM_ID_alter() for install_configure_form().
   *
   * Allows the profile to alter the site configuration form.
   */
  function phpedu_profile_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
    // Add a value as example that one can choose an arbitrary site name.
    $form['site_information']['site_name']['#placeholder'] = t('University of Demo');
  }

  /**
   * Install task callback; prepares a batch job to install extensions.
   *
   * @param array $install_state
   *   The current install state.
   *
   * @return array
   *   The batch job definition.
   */
  function phpedu_profile_install_demo(array &$install_state) {
    $batch = array();
    foreach ($install_state['phpedu_profile']['modules'] as $module) {
      $batch['operations'][] = ['phpedu_profile_install_module', (array) $module];
    }
    return $batch;
  }

  /**
   * Batch API callback. Installs a module.
   *
   * @param string|array $module
   *   The name(s) of the module(s) to install.
   */
  function phpedu_profile_install_module($module) {
    \Drupal::service('module_installer')->install((array) $module);
  }

  /**
   * Implements template_preprocess_block().
   */
  function phpedu_profile_preprocess_block(array &$variables) {
    $variables['attributes']['data-block-plugin-id'] = $variables['elements']['#plugin_id'];
  }
