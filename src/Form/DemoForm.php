<?php

  namespace Drupal\phpedu_profile\Form;

  use Drupal\Core\Form\FormBase;
  use Drupal\Core\Form\FormStateInterface;
  use Drupal\Core\Render\Element\Form;
  use Symfony\Component\DependencyInjection\ContainerInterface;

  class DemoForm extends FormBase {

    /**
     * Returns a unique string identifying the form.
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId() {
      return 'phpedu_demo';
    }

    /**
     * Form constructor.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
      $form['#title'] = $this->t('Extensions');

      // Add 'phpedu' fieldset and demo options.
      $form['phpedu'] = [
        '#type' => 'details',
        '#title' => t('Demo University Content'),
        '#description' => $this->t('Enable some extra features for better media handling and authoring experience. Also, optionally,  populate the site with content to demonstrate phpEdu features. You may revert this demo content later.'),
        '#weight' => -5,
        '#open' => TRUE,
      ];

      // Checkboxes to enable demo features.
      $form['phpedu']['extensions'] = [
        '#type' => 'checkboxes',
        '#options' => [
          'lightning_media' => $this->t('Lightning Media'),
          'lightning_layout' => $this->t('Lightning Layout'),
          'lightning_workflow' => $this->t('Lightning Workflow'),
          'pe_migrate' => t('Configure demo content'),
        ],
        '#default_value' => ['pe_migrate', 'lightning_media', 'lightning_layout', 'lightning_workflow'],
      ];

      $form['actions'] = [
        'continue' => [
          '#type' => 'submit',
          '#value' => $this->t('Continue'),
        ],
        '#type' => 'actions',
      ];

      return $form;

    }

    /**
     * Form submission handler.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      $modules = array_filter($form['phpedu']['extensions']['#value']);

      if (in_array('lightning_media', $modules)) {
        $modules[] = 'lightning_media_document';
        $modules[] = 'lightning_media_image';
        $modules[] = 'lightning_media_instagram';
        $modules[] = 'lightning_media_twitter';
        $modules[] = 'lightning_media_video';
      }

      $GLOBALS['install_state']['phpedu_profile']['modules'] = $modules;
    }
  }
